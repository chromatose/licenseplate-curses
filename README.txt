INSTRUCTIONS
------------
Please run this in a terminal that is at least 80x24 (length x height).

Also, please keep the README.txt in the same directory as the executable.

Compile with -lcurses, -lm flags.


OVERVIEW
---------
My program should fulfill all of the basic requirements.

Some of the bonus requirements I fulfilled:
- Splash screen
- Bold, colored text
- Advanced input validation 
License plate input automatically gets uppercased.
Both plate input and date input go character-by-character.
For the dates, the month proactively prevents you from entering
an invalid date (e.g. if you type a '1', you can only form 10, 11, 12).
- Visualization of calculating the plates
(I'm rather proud of that one)

General creativity:
- A message box that tells you what you can do next, and gives you
feedback if you select a wrong option or send a signal
- Fully menu-driven system
- Placeholder text that uses a different font than input
- Placeholder slash (/) between month and year
- You can backspace on your input, and the backspace preserves 
placeholder text
- If your terminal supports it, invisible cursor whenever there's 
no field to fill out [curs_set()]

Other:
- Can be compiled with -Wall with no warning
- Program will not show the goodbye ASCII if exited thru a signal

I spent way too much time on this. 


ASCII
-----

splash
88                                     8""""8                              
88    e  eeee eeee eeeee eeeee eeee    8    8 e     eeeee eeeee eeee eeeee 
88    8  8  8 8    8   8 8   " 8       8eeee8 8     8   8   8   8    8   " 
88    8e 8e   8eee 8e  8 8eeee 8eee    88     8e    8eee8   8e  8eee 8eeee 
88    88 88   88   88  8    88 88      88     88    88  8   88  88      88 
88eee 88 88e8 88ee 88  8 8ee88 88ee    88     88eee 88  8   88  88ee 8ee88 
$
instructions
 _____           _                   _   _                 
|_   _|         | |                 | | (_)                
  | |  _ __  ___| |_ _ __ _   _  ___| |_ _  ___  _ __  ___ 
  | | | '_ \/ __| __| '__| | | |/ __| __| |/ _ \| '_ \/ __|
 _| |_| | | \__ \ |_| |  | |_| | (__| |_| | (_) | | | \__ \
|_____|_| |_|___/\__|_|   \__,_|\___|\__|_|\___/|_| |_|___/
$
start
 _____ _             _   _              ______ _       _       
/  ___| |           | | (_)             | ___ \ |     | |      
\ `--.| |_ __ _ _ __| |_ _ _ __   __ _  | |_/ / | __ _| |_ ___ 
 `--. \ __/ _` | '__| __| | '_ \ / _` | |  __/| |/ _` | __/ _ \
/\__/ / || (_| | |  | |_| | | | | (_| | | |   | | (_| | ||  __/
\____/ \__\__,_|_|   \__|_|_| |_|\__, | \_|   |_|\__,_|\__\___|
                                  __/ |     
                                 |___/              
$
end
 _____          _ _              ______ _       _       
|  ___|        | (_)             | ___ \ |     | |      
| |__ _ __   __| |_ _ __   __ _  | |_/ / | __ _| |_ ___ 
|  __| '_ \ / _` | | '_ \ / _` | |  __/| |/ _` | __/ _ \
| |__| | | | (_| | | | | | (_| | | |   | | (_| | ||  __/
\____/_| |_|\__,_|_|_| |_|\__, | \_|   |_|\__,_|\__\___|
                           __/ |                        
                          |___/                         
$
unknown
 _   _       _                               ______ _       _       
| | | |     | |                              | ___ \ |     | |      
| | | |_ __ | | ___ __   _____      ___ __   | |_/ / | __ _| |_ ___ 
| | | | '_ \| |/ / '_ \ / _ \ \ /\ / / '_ \  |  __/| |/ _` | __/ _ \
| |_| | | | |   <| | | | (_) \ V  V /| | | | | |   | | (_| | ||  __/
 \___/|_| |_|_|\_\_| |_|\___/ \_/\_/ |_| |_| \_|   |_|\__,_|\__\___|
$
distribution
______ _     _        _ _           _   _             
|  _  (_)   | |      (_) |         | | (_)            
| | | |_ ___| |_ _ __ _| |__  _   _| |_ _  ___  _ __  
| | | | / __| __| '__| | '_ \| | | | __| |/ _ \| '_ \ 
| |/ /| \__ \ |_| |  | | |_) | |_| | |_| | (_) | | | |
|___/ |_|___/\__|_|  |_|_.__/ \__,_|\__|_|\___/|_| |_|
$
bye
 _____                 _  ______            _ 
|  __ \               | | | ___ \          | |
| |  \/ ___   ___   __| | | |_/ /_   _  ___| |
| | __ / _ \ / _ \ / _` | | ___ \ | | |/ _ \ |
| |_\ \ (_) | (_) | (_| | | |_/ / |_| |  __/_|
 \____/\___/ \___/ \__,_| \____/ \__, |\___(_)
                                  __/ |       
                                 |___/        
$
