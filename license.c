/*
Andres Santana, ars5964
CMPSC 474/CMPEN441
Homework 1
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

/* Constants */
#define PSIZE 7

/* Hold the date */
typedef struct date
{
	int month;
	int year;
} date;


/* Plate related functions */
bool read_plate(char* newplt);
int get_plate_difference(char* one, char* two);

/* Date related functions */ 
bool read_date(date* newdate);
int get_month_difference(date* one, date* two);
date calc_new_date(int plates, date* start, float rate);

int main(int *argc, char *argv[]) 
{
	/* Plates */
	char plt_one[PSIZE];
	char plt_two[PSIZE];
	char plt_unk[PSIZE];

	/* Dates */
	date d_one, d_two, d_unk;

	/* Differences between two plates and dates */
	int pdiff, ddiff;

	/* User enters plate 1 information */
	printf("Enter the starting license plate: ");
	while(!read_plate(plt_one))
		printf("Error, the format is 3 letters followed by 4 digits. Try again: ");
	printf("Enter the date of creation (MM/YYYY format): ");
    while(!read_date(&d_one))
		printf("Error, please enter a valid date (MM/YYYY format): ");

	/* User enters plate 2 information */
	printf("Enter the second license plate: ");
	while(!read_plate(plt_two))
		printf("Error, the format is 3 letters followed by 4 digits. Try again: ");
	printf("Enter the date of creation (MM/YYYY format): ");
    while(!read_date(&d_two))
		printf("Error, please enter a valid date (MM/YYYY format): "); 


	/* Get plate and date differences */
	pdiff = get_plate_difference(plt_one, plt_two);
	ddiff = get_month_difference(&d_one, &d_two);

	double rate = (double)pdiff/ddiff;

	printf("In %d months, %d plates were distributed.\n", ddiff, pdiff);
	printf("This comes out to %f plates per month.\n", rate);

	/* Enter unknown plate information */
	printf("Enter the unknown license plate number: ");
	while(!read_plate(plt_unk))
		printf("Error, the format is 3 letters followed by 4 digits. Try again: ");
	
	/* Get starting and unknown plate differences */
	pdiff = get_plate_difference(plt_one, plt_unk);
	d_unk = calc_new_date(pdiff, &d_one, rate);
	
	printf("Between the starting and unknown plate, there were %d plates.\n", pdiff);
	printf("This means that the unknown plate will be distributed on %02d/%4d.\n",
			d_unk.month, d_unk.year);
}

/* Returns true if a valid plate array was created, false otherwise */
bool read_plate(char* p)
{
	/* License plates are 7 characters long */
	const size_t SIZE = 7;

	size_t len = 0;
	char* raw = NULL;
	
	/* Get input from user */
	size_t numchars = getline(&raw, &len, stdin);
	
	/* Remove newline if it exists */
	if (raw[numchars-1] == '\n') 
	{
		raw[numchars-1] == '\0';
		numchars--;
	}

	/* Ensure that the size of the input was correct */
	if (numchars != SIZE)
		return false;
	
	/* Ensure that the format of the input was correct */
	/* If it is, store it in p */
	int iter;
	for (iter = 0; iter < 3; ++iter) 
	{
		/* Return if the input character is not a letter*/
		if (!isalpha(raw[iter]) || 
			(raw[iter] < 'a' && raw[iter] > 'z') ||
			(raw[iter] < 'A' && raw[iter] > 'Z')  )
			return false; 
		
		/* Convert plate to all uppercase */
		if (islower(raw[iter]))
			raw[iter] = toupper(raw[iter]);

		p[iter] = raw[iter];
	}

	/* Ensure the numerical part really is numerical */
	for (; iter < PSIZE; ++iter) {
		if (!isdigit(raw[iter]))
			return false;
		p[iter] = raw[iter];
	}

	return true;
}

/* Returns true if a valid date struct was created, false otherwise */
bool read_date(date* d) 
{
	/* MM/YYYY is 7 characters long */
	const size_t SIZE = 7; 

	size_t len = 0;
	char* rawdate = NULL;

	size_t numchars = getline(&rawdate, &len, stdin);
	
	/* Remove newline */
	if (rawdate[numchars-1] == '\n')
	{
		rawdate[numchars-1] == '\0';
		numchars--;
	}
	
	/* Ensure that the size of input was correct */
	if (numchars != SIZE)
		return false;
	
	/* Check if the input has proper formatting */
	for (int i = 0; i < SIZE; ++i)
	{
		/* Account for the slash */
		if (i == 2 && rawdate[i] == '/')
			continue;

		if (!isdigit(rawdate[i]))
			return false;
	}

	/* Get numerical month and year */
	int month = atoi(rawdate);
	int year = atoi(rawdate + 3);
	
	/* Check to see that month makes sense */
	if (month < 1 || month > 12 )
		return false;
	
	d->month = month;
	d->year = year;

	return true;
}

/* Find the number of plates that occurred between the input plates */
int get_plate_difference(char* p_one, char* p_two) 
{
	/* Make a copy of the first plate to avoid destruction of data */
	char plt_cpy[PSIZE];
	strncpy(plt_cpy, p_one, PSIZE);
	
	/* True when digit exceeds 9 or char exceeds Z */
	bool was_incremented = false;
	
	/* Hold the value to be returned*/
	int difference = 0;

	/* Keep track of the current place to be incremented */
	/* e.g. ones place or tens place */
	int place = PSIZE - 1;

	/* Run until plates are equal */
	while (strncmp(plt_cpy, p_two, PSIZE) != 0) 
	{
		was_incremented = false;

		/* Perform wrapping if about to exceed size */
		if (strncmp(plt_cpy, "ZZZ9999", PSIZE) == 0)
		{
			strncpy(plt_cpy, "AAA0000", PSIZE);
			++difference;
			continue;
		} 

		/* Handle digit incrementation */
		if (place > 2) 
		{
			if (plt_cpy[place] < '9')	
			{
			++plt_cpy[place];
			++difference;
			}
			else
			{
			plt_cpy[place] = '0';
				was_incremented = true;
			}
		}
		/* Handle character incrementation */
		else
		{
			if (plt_cpy[place] < 'Z')
			{
				++plt_cpy[place];
				++difference;
			}
			else 
			{ 
				plt_cpy[place] = 'A';
				was_incremented = true;
			}
		}

		/* Increase place if carryover occurs; otherwise return to ones place */
		place = (was_incremented) ? place - 1 : PSIZE - 1;
	}
	return difference;
}

/* Calculate the difference in months between two dates */
int get_month_difference(date* one, date* two)
{
	int year, month;
	year = two->year - one->year;
	month = two->month - one->month;
	return month + year * 12;
}

/* Calculate the date in which a new plate will be released */
date calc_new_date(int plates, date* start, float rate)
{
	/* Initialize the calculated date with the starting date */
	date newdate;
	newdate.month = start->month;
	newdate.year = start->year;

	/* Get the number of months elapsed */
	int months = round((float)plates/rate);

	/* Convert months elapsed to years */
	while (months > 12)
	{
		months -= 12;
		++newdate.year;
	}

	/* Add leftover months into the newdate */

	/* If addition exceeds months, add a new year and start the months over */
	if (newdate.month + months > 12)
	{
		++newdate.year;
		newdate.month = (newdate.month + months) % 12;
	}
	/* Otherwise just add the two months together */
	else
		newdate.month = newdate.month + months;

	return newdate;
}
