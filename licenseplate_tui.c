/* 
Andres Santana, ars5964
License plate program enhanced 
*/
#include <curses.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <signal.h>

#define PSIZE 8
#define DSIZE 8

/* ASCII file constants */ 
static const char* ASCII_FILE = "README.txt";
static const char* ASCII_DELIM = "$";

/* Hold the date */
typedef struct date
{
	int month;
	int year;
} date;

/* Plate types */
typedef enum PTYPE {START = 0, END = 1, UNKNOWN = 2} PTYPE;

/* Coordinate system for keeping track of cursor position */
enum CORD {X = 0, Y};


/*** UI FUNCTIONS */

/* Print grey hinting text */
void greyprintw(int y, int x, char* msg);

/* Output the ASCII art corresponding to a given ID at supplied coordinates
   Returns: width, or -1 if id is invalid */
int draw_ASCII(char* id, int y, int x);

/* Create a message box window at the supplied y-coordinate */
WINDOW* message;
WINDOW* create_message_box(int y_offset, int len);

/* Set a message box's message*/
void set_message(char* text);

/* Draw the main menu of the program at the given position*/
void draw_menu(int y, int x);

/* Draws a box with the given width, height, and starting coordinates */
void draw_box(int height, int width, int y, int x);

/* Other screens */
bool run_plate(char* plt, date* dt, PTYPE p);
int run_difference(char* s_plt, char* e_plt, date* s_dt, date* e_dt);
void run_unknown(char* s_plt, date* s_dt, int rate);


/*** PLATE/DATE FUNCTIONS */

/* Plate related functions */
void read_plate(char* newplt, int y, int x);
int get_plate_difference(char* one, char* two, int x, int y);

/* Date related functions */ 
void read_date(date* newdate, int y, int x);
int get_month_difference(date* one, date* two);
date calc_new_date(int plates, date* start, float rate);

/* Reroute signals to output to the message box */
void sig_handler(int sig)
{
    int c;
    set_message("Do you really want to quit? [y/n] ");

    c = tolower(getch());
    if (c == 'y')
    {
        refresh();
        endwin();
        exit(EXIT_SUCCESS);
    }
    else {
        set_message("Okay, carry on then.");
    }
}
/*** MAIN */
int main(int argc, char* argv[])
{
    /* Signal handler override */
    (void) signal(SIGINT, sig_handler);
    (void) signal(SIGQUIT, sig_handler);
    (void) signal(SIGTSTP, sig_handler);


    /* Initialization functions */
    initscr();              /* curses mode */
    noecho();               /* don't echo input back to user */
    curs_set(0);            /* make cursor invisible */
    start_color();	        /* enable color */
    use_default_colors();   /* default colors */
    cbreak();
    
    /* UI element positions */
    int menu_pos[] = {20, 6};

    /* Hold the starting license plate information */
    bool start_retrieved = false;
    char start_plate[PSIZE];
    date start_date;

    /* Hold the ending license plate information */
    bool end_retrieved = false;
    char end_plate[PSIZE];
    date end_date;

    /* Hold the unknown plate information */
    char unk_plate[PSIZE];

    /* Hold rate information */
    int rate = 0;
    bool is_rate_current;

    memset(start_plate, '\0', sizeof(start_plate));
    memset(end_plate, '\0', sizeof(end_plate));
    memset(unk_plate, '\0', sizeof(unk_plate));

    /* Draw ASCII splash */
    int length_banner = draw_ASCII("splash", 0, 0);

    /* Draw the menu */
    draw_menu(menu_pos[Y], menu_pos[X]);

    /* Create message box */
    if (length_banner < 20)
    	message = create_message_box(19, 75);
    else
        message = create_message_box(19, length_banner);
    set_message("Press any of the colored keys to advance.");

    refresh();
    wrefresh(message);

    /* Set an alternate message if necessary */
    bool print_alt = false;
    char alt_message[80];
    memset(alt_message, '\0', sizeof(alt_message));

    /* Main input loop */
    int input;    
    do 
    {
        /* Clear and refresh the screen at the beginning of each cycle */
        clear();
        refresh();

        /* Redraw everything once we come back to main menu */
        draw_ASCII("splash", 0, 0);
        draw_menu(menu_pos[Y], menu_pos[X]);

        if (print_alt)
        {
            set_message(alt_message);
            print_alt = false;
        }
        else 
            set_message("Press any of the colored keys to advance.");

        /* Menu selection */
        input = getch();
        /* Clear and refresh before moving to submenu */
        clear();
        refresh();
        switch (tolower(input))
        {
        /* Starting plate */
        case 'a':
            start_retrieved = run_plate(start_plate, &start_date, START);
            is_rate_current = false;
            break;
        /* Ending plate */
        case 'b': 
            end_retrieved = run_plate(end_plate, &end_date, END);
            is_rate_current = false;
            break;
        /* Start and end comparsion */
        case 'c':
            if (start_retrieved && end_retrieved)
            {
                rate = run_difference(start_plate, end_plate, &start_date, &end_date);
                is_rate_current = true;
            }
            else
            {
                strcpy(alt_message, "This option requires both a starting and ending plate.");
                print_alt = true;
            }
            break;
        /* Unknown comparison */
        case 'd':
            if (is_rate_current)
            {
                run_unknown(start_plate, &start_date, rate);
            }
            else
            {
                strcpy(alt_message, "This option requires an updated rate; you must (re)run option c.");
                print_alt = true;
            }
            break;
        /* Exit page */
        case 'q':
            clear();
            refresh();
            init_pair(3, COLOR_MAGENTA, -1);
            attron(A_BOLD);
            attron(COLOR_PAIR(3));
            draw_ASCII("bye", 8, 14);
            getch();
            break;
        /* Invalid input */
        default:
            strcpy(alt_message, "That isn't a valid option.");
            print_alt = true;
            break;
        }

        
    } while(input != 'q');

    refresh();
    endwin();
    exit(EXIT_SUCCESS);
}


int draw_ASCII (char* id, int y, int x)
{
    
    bool found = false;
    int largest_line = 0;

    /* Open file for reading */
    FILE* fp = fopen(ASCII_FILE, "r");
    if (fp == NULL)
        return -1;
    
    /* Read line by line */
    char* line = NULL;
    size_t len = 0;
    ssize_t read;
    
    /* Find the ASCII art ID within the file */
    while((read = getline(&line, &len, fp)) != -1)
    {
        /* Quit if tag was found */
        if (strncmp(id, line, strlen(id)) == 0)
        {
            found = true;
            break;
        }
    }
    
    
    /* If ID is found, print section of ASCII line-by-line */
    if (found)
    {
        move(y, x);
        while((read = getline(&line, &len, fp)) != -1 )
        {
            /* Exit if delimiting character is encountered */
            if (strncmp(line, ASCII_DELIM, strlen(ASCII_DELIM)) == 0)
                break;

            /* Print the next line */
            move(y++, x);
            printw("%s", line);
            
            /* Find the largest line */
            largest_line = read > largest_line ? read: largest_line;
            
        }
    }
    
    fclose(fp);
    return largest_line;
}

void draw_menu(int y, int x)
{
    /* Menu strings */
    int num_menu = 5;
    char*  menu_opt[]    = {"a)", "b)", "c)", "d)", "q)"};
    char*  menu_string[] = {"Enter starting plate",
                            "Enter ending plate",
                            "Compare starting and ending plate",
                            "Find unknown license plate",
                            "Quit the program"};


    /* Blue color for normal items */
    init_pair(1, COLOR_BLUE, -1);

    /* Red color for quit */
    init_pair(2, COLOR_RED, -1);
    int i;
    for (i = 0; i < num_menu; ++i)
    {
        /* Print the option using bold and a color */
        attron(A_BOLD);
        attron( (i == (num_menu - 1) )? COLOR_PAIR(2): COLOR_PAIR(1));
        mvprintw(y + 2*(i+1), x, menu_opt[i]);
        attroff(A_BOLD);
        attroff( (i == (num_menu - 1) ) ? COLOR_PAIR(3): COLOR_PAIR(1));

        /* Print the menu item */
        mvprintw(y+ 2*(i+1), x + 3, menu_string[i]);
    }
}

WINDOW* create_message_box(int y_offset, int len)
{
    WINDOW* messagebox = newwin(3, len, y_offset, 0);
    box(messagebox, ACS_VLINE, ACS_HLINE);
    return messagebox;
}

void set_message(char* text)
{
    wclear(message);
    box(message, ACS_VLINE, ACS_HLINE);
    mvwprintw(message, 1, 1, text);
    wrefresh(message);
}

void draw_box(int height, int width, int y, int x)
{
    /* Print top and left sides */
    move(y, x);
    hline(ACS_HLINE, width);
    vline(ACS_VLINE, height);

    /* Print right side */
    move(y, x+ width);
    vline(ACS_VLINE, height);

    /* Print bottom side */
    move(y + height, x);
    hline(ACS_HLINE, width);

    /* Print top left edge */
    move(y, x);
    hline(ACS_ULCORNER, 1);

    /* Print top right edge */
    move(y, x + width);
    hline(ACS_URCORNER, 1);

    /* Print bottom left edge */
    move(y + height, x);
    hline(ACS_LLCORNER, 1);

    /* Print bottom right edge */
    move(y + height, x + width);
    hline(ACS_LRCORNER, 1);
}

bool run_plate(char* plt, date* dt, PTYPE pt)
{
    /* UI element positions */
    int plate_pos[] = {20, 10};
    int date_pos[] = {28, 13};

    /* Draw ASCII */
    if (pt == START)
        draw_ASCII("start", 0, 6);
    else if (pt == END)
        draw_ASCII("end", 0, 10);

    /* Draw plate UI elements */
    mvprintw(plate_pos[Y], plate_pos[X], "Plate number:");
    draw_box(2, 9, plate_pos[Y] - 1, plate_pos[X] + 14);

    /* Draw date UI elements */
    mvprintw(date_pos[Y], date_pos[X], "Date:");
    draw_box(2, 9, date_pos[Y] -1, date_pos[X] + 6);
    
    /* Print instructions */
    mvprintw(18, 15, "L - letter   N - number   M - month   Y - year");

    /* Draw placeholder grey elements*/
    greyprintw(plate_pos[Y], plate_pos[X] + 15, "LLLNNNN");
    greyprintw(date_pos[Y], date_pos[X] + 7, "MM");
    mvprintw(date_pos[Y], date_pos[X] + 9, "/");
    greyprintw(date_pos[Y], date_pos[X] + 10, "YYYY");
    
    
    /* Read the plate */
    read_plate(plt, plate_pos[Y], plate_pos[X] + 15);
    /* Read the date */
    read_date(dt, date_pos[Y], date_pos[X] + 7);

    return true;
}

int run_difference(char* s_plt, char* e_plt, date* s_dt, date* e_dt)
{
    /* Store differences */
    int pdiff = 0;
    int ddiff = 0;
    double rate = 0;

    /* UI element positions */
    int pstart_pos[] = {0, 7};
    int pend_pos[] = {0, 10};
    int pcalc_pos[] = {0, 13};
    int y_final = 16;

    /* Draw ASCII */
    draw_ASCII("distribution", 0, 10);


    /*** UI LAYOUT */

    /* Start plate */
    mvprintw(pstart_pos[Y], pstart_pos[X], "The starting plate is");
    pstart_pos[X] += strlen("The starting place is ");
    draw_box(2, 9, pstart_pos[Y] - 1, pstart_pos[X]);
    mvprintw(pstart_pos[Y], ++pstart_pos[X], s_plt);
    pstart_pos[X] += 10;
    mvprintw(pstart_pos[Y], pstart_pos[X], "issued on");
    pstart_pos[X] += strlen("issued on");
    mvprintw(pstart_pos[Y], ++pstart_pos[X], "%d/%d. ", s_dt->month, s_dt->year);
    /* End plate */
    mvprintw(pend_pos[Y], pend_pos[X], "The ending plate is");
    pend_pos[X] += strlen("The ending place is ");
    draw_box(2, 9, pend_pos[Y] - 1, pend_pos[X]);
    mvprintw(pend_pos[Y], ++pend_pos[X], e_plt);
    pend_pos[X] += 10;
    mvprintw(pend_pos[Y], pend_pos[X], "issued on");
    pend_pos[X] += strlen("issued on");
    mvprintw(pend_pos[Y], ++pend_pos[X], "%d/%d. ", e_dt->month, e_dt->year);

    /* Show the calculations between plates */
    mvprintw(pcalc_pos[Y], pcalc_pos[X], "Calculating:");
    pcalc_pos[X] += strlen("Calculating: ");
    draw_box(2, 9, pcalc_pos[Y] - 1, pcalc_pos[X]);

    /* Set message */
    set_message("Press any key to calculate the difference between plates.");
    getch();
    set_message("Please do not press anything while the calculation is being performed.");
    mvprintw(pcalc_pos[Y], ++pcalc_pos[X], s_plt);
    
    pdiff = get_plate_difference(s_plt, e_plt, pcalc_pos[X], pcalc_pos[Y]);
	ddiff = get_month_difference(s_dt, e_dt);
    rate = (double)pdiff/ddiff;

    mvprintw(y_final, 0, "In %d months, %d plates were distributed.", ddiff, pdiff);
    mvprintw(y_final + 1, 0, "This comes out to %f plates per month.", rate);

    /* Prepare for return */
    refresh();
    set_message("Press any key to return to the main menu.");
	getch();

    return rate;
}

void run_unknown(char* s_plt, date* s_dt, int rate)
{
    /* Store differences */
    int pdiff = 0;
    date d_unk;

    /* Unknown plate */
    char unk_plate[PSIZE];
    memset(unk_plate, '\0', sizeof(unk_plate));

    /* Draw ASCII */
    draw_ASCII("unknown", 0, 2);

    /* UI element positions */
    int unk_pos[] = {0, 9};

    /*** UI LAYOUT */
    /* Start plate */
    mvprintw(unk_pos[Y], unk_pos[X], "Calculating the difference between");
    unk_pos[X] += strlen("Calculating the difference between ");
    draw_box(2, 9, unk_pos[Y] - 1, unk_pos[X]);
    mvprintw(unk_pos[Y], ++unk_pos[X], s_plt);

    /* Unknown plate */
    unk_pos[X] += 10;
    mvprintw(unk_pos[Y], unk_pos[X], "and");
    unk_pos[X] += strlen("and ");
    draw_box(2, 9, unk_pos[Y] - 1, unk_pos[X]);

    /* Draw placeholder grey elements*/
    greyprintw(unk_pos[Y], ++unk_pos[X], "LLLNNNN");

    /* Draw key at bottom before message */
    mvprintw(unk_pos[Y] + 9, 20, "L - letter   N - number");

    /* Show the calculations */
    mvprintw(unk_pos[Y], unk_pos[X] + 10, ":");
    draw_box(2, 9, unk_pos[Y] -1, unk_pos[X] + 12);

    /* Get unknown plate */
    read_plate(unk_plate, unk_pos[Y], unk_pos[X]);
    set_message("Please do not press anything while the calculation is being performed.");
    /* Get plate differences */
    pdiff = get_plate_difference(s_plt, unk_plate, unk_pos[X] + 13, unk_pos[Y]);
    d_unk = calc_new_date(pdiff, s_dt, rate);

    mvprintw(unk_pos[Y] + 3, 0, "Between %s and %s, there were %d plates.", 
        s_plt, unk_plate, pdiff);
    mvprintw(unk_pos[Y] + 6, 0, "This means that the unknown plate will be distributed on %02d/%4d.",
        d_unk.month, d_unk.year);

    set_message("Press any key to return to the main menu.");
    getch();
}

void read_plate(char* p, int y, int x)
{
    /* Save the start of the input */
    int start = x;

    /* Initialize cbreak mode for input processing */
    curs_set(1);

    set_message("Please type the plate in the specified format, then press ENTER.");

	/* Get input from user */
	int iter = 0;
	while(iter < PSIZE)
	{
        move(y, x);
		p[iter] = getch();
        set_message("Please type the plate in the specified format, then press ENTER.");

        /* Handle backspace */
        if (p[iter] == KEY_BACKSPACE || p[iter] == KEY_DC || p[iter] == 127)
        {
            if (x > start)
            {
                p[iter] = '\0';
                iter--;
                x--;

                /* Restore character hint */
                if (iter < 3)
                    greyprintw(y, x, "L");
                else   
                    greyprintw(y, x, "N");               
            }
            else
            {
                set_message("Can't go back any further than this.");
            }
            
            continue;
        }
        /* Handle enter */
        if (p[iter] == '\n' && iter < PSIZE)
        {
            if (iter < PSIZE - 1)
                set_message("Please only type ENTER when you're finished, not before.");
            else
                iter++;
            continue;
        }

        /* Character validation on first half of license plate */
        if (iter < 3)
        {
            /* Is input a character */
            if (isalpha(p[iter]))
            {
                p[iter] = toupper(p[iter]);
                addch(p[iter]);
                x++;
                iter++;
            }
            /* Is input a non-digit */
            else
            {
                set_message("That wasn't a letter, try again.");
            }
        }
        /* Digit validation on second half of license plate */
        else if (iter >= 3 && iter < 7)
        {
            /* Is input a digit */
            if (isdigit(p[iter]))
            {
                addch(p[iter]);
                x++;
                iter++;
            }
            /* Is input a non-digit */
            else 
            {
                set_message("That wasn't a number, try again.");
            }
        }
        /* The end of the license plate input */
        else 
        {
            set_message("Press ENTER if you're happy with your plate.");
        }
	}
    p[PSIZE-1] = '\0';
    curs_set(0);
}

void read_date(date* d, int y, int x)
{
    /* Save the start of the input */
    int start = x;
    int slash_pos = 2;

    /* Store the raw character data */
    char rawdate[DSIZE];
    memset(rawdate, '\0', sizeof(rawdate));
    rawdate[slash_pos] = '/';

	/* Initialize cbreak mode for input processing */
    curs_set(1);

    set_message("Please type the date in the specified format, then press ENTER.");

	/* Get input from user */
	int iter = 0;
	while(iter < DSIZE)
	{
        /* Handle the slash that's in between the month and year */
        if (iter == slash_pos)
            move(y, ++x);
        else
            move(y, x);
        
		rawdate[iter] = getch();
        set_message("Please type the date in the specified format, then press ENTER.");

        /* Handle backspace */
        if (rawdate[iter] == KEY_BACKSPACE || rawdate[iter] == KEY_DC || rawdate[iter] == 127)
        {
            if (x > start)
            {
                rawdate[iter] = '\0';

                /* Go back two spaces to avoid the slash */
                if (iter == slash_pos + 1)
                {
                    iter -= 2;
                    x -= 2;
                }
                /* Otherwise, just go back one space */
                else
                {
                    iter--;
                    x--;
                }

                /* Restore character hint */
                if (iter < 2)
                    greyprintw(y, x, "M");
                else if (iter > 2 && iter < DSIZE)  
                    greyprintw(y, x, "Y");               
            }
            else
            {
                set_message("Can't go back any further than this.");
            }
            
            continue;
        }

        /* Handle enter */
        if (rawdate[iter] == '\n' && iter < DSIZE)
        {
            if (iter < DSIZE - 1)
                set_message("Only type ENTER when you're finished, not before.");
            else
                iter++;
            continue;
        }

        /* Handle non-digit entries */
        if (!isdigit(rawdate[iter]))
        {
            set_message("That was not a number, try again.");
            continue;
        }

        /* Month validation */
        if (iter < slash_pos)
        {
            if (iter == 0 && (rawdate[iter] == '0' || rawdate[iter] == '1') )
            {
                addch(rawdate[iter]);
                x++;
                iter++;
            }
            else if (iter == 1 && (
                    (rawdate[0] == '0' && rawdate[iter] >= '1' && rawdate[iter] <= '9') ||
                    (rawdate[0] == '1' && rawdate[iter] >= '0' && rawdate[iter] <= '2') ))
            {
                addch(rawdate[iter]);
                x += 2;
                iter += 2;
            }
            else {
                    set_message("This will not lead to a valid month.");
            }
        }
        /* Year doesn't need validation because we already handled non-digits*/
        else if (iter > slash_pos && iter < DSIZE - 1)
        {
            addch(rawdate[iter]);
            x++;
            iter++;
        }
        else
        {
            set_message("Press ENTER if you're happy with your date.");
        }
	}
    
    /* Disable the opts */
    cbreak();
    curs_set(0);

	/* Get numerical month and year */
	int month = atoi(rawdate);
	int year = atoi(rawdate + 3);
	
	
	d->month = month;
	d->year = year;
}

/* Find the number of plates that occurred between the input plates */
int get_plate_difference(char* p_one, char* p_two, int x, int y) 
{
	/* Make a copy of the first plate to avoid destruction of data */
	char plt_cpy[PSIZE];
	strncpy(plt_cpy, p_one, PSIZE);
	
	/* True when digit exceeds 9 or char exceeds Z */
	bool was_incremented = false;
	
	/* Hold the value to be returned*/
	int difference = 0;

	/* Keep track of the current place to be incremented */
	/* e.g. ones place or tens place */
	int place = PSIZE - 2;

	/* Run until plates are equal */
	while (strncmp(plt_cpy, p_two, PSIZE) != 0) 
	{
		was_incremented = false;

		/* Perform wrapping if about to exceed size */
		if (strncmp(plt_cpy, "ZZZ9999", PSIZE) == 0)
		{
			strncpy(plt_cpy, "AAA0000", PSIZE);
			++difference;
			continue;
		} 

		/* Handle digit incrementation */
		if (place > 2) 
		{
			if (plt_cpy[place] < '9')	
			{
                ++plt_cpy[place];
                ++difference;
			}
			else
			{
			plt_cpy[place] = '0';
				was_incremented = true;
			}
		}
		/* Handle character incrementation */
		else
		{
			if (plt_cpy[place] < 'Z')
			{
				++plt_cpy[place];
				++difference;
			}
			else 
			{ 
				plt_cpy[place] = 'A';
				was_incremented = true;
			}
		}
        /* Rapidly print the license plates */
        if (difference % 450 == 0)
        {
            mvprintw(y, x, plt_cpy);
            refresh();
            usleep(1);
        }

		/* Increase place if carryover occurs; otherwise return to ones place */
		place = (was_incremented) ? place - 1 : PSIZE - 2;
	}
    mvprintw(y, x, plt_cpy);
    refresh();

	return difference;
}

/* Calculate the difference in months between two dates */
int get_month_difference(date* one, date* two)
{
	int year, month;
	year = two->year - one->year;
	month = two->month - one->month;
	return month + year * 12;
}

/* Calculate the date in which a new plate will be released */
date calc_new_date(int plates, date* start, float rate)
{
	/* Initialize the calculated date with the starting date */
	date newdate;
	newdate.month = start->month;
	newdate.year = start->year;

	/* Get the number of months elapsed */
	int months = round((float)plates/rate);

	/* Convert months elapsed to years */
	while (months > 12)
	{
		months -= 12;
		++newdate.year;
	}

	/* Add leftover months into the newdate */

	/* If addition exceeds months, add a new year and start the months over */
	if (newdate.month + months > 12)
	{
		++newdate.year;
		newdate.month = (newdate.month + months) % 12;
	}
	/* Otherwise just add the two months together */
	else
		newdate.month = newdate.month + months;

	return newdate;
}

void greyprintw(int y, int x, char* msg)
{
    attron(COLOR_PAIR(1));
    mvprintw(y, x, msg);
    attroff(COLOR_PAIR(1));
}
